import getIdentity from '../src/index';
import config from './config.json'; 
import { PostgreSqlEngine } from '@digigov-oss/gsis-audit-record-db';
const test = async () => {
    try {
        const overrides = {
        auditEngine: new PostgreSqlEngine('postgres://postgres:postgres@localhost:5432/postgres'),
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        }
        const Identity = await getIdentity("052704062", config.user, config.pass, overrides);
        return Identity;
    } catch (error) {
        console.log(error);
    }
}

test().then((identity) => { console.log('getNncIdentityOutputRecord',identity); });