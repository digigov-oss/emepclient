import { AuditRecord, AuditEngine } from "@digigov-oss/gsis-audit-record-db";
export declare type AuditInit = AuditRecord;
export declare type GetNncIdentityOutputRecord = {
    countryCode?: string;
    countryDescr?: string;
    addressStreet?: string;
    addressNumber?: string;
    addressCity?: string;
    addressZipCode?: string;
    telephone?: string;
    countryCode2?: string;
    countryDescr2?: string;
    addressStreet2?: string;
    addressNumber2?: string;
    addressCity2?: string;
    addressZipCode2?: string;
    telephone2?: string;
    mobile?: string;
    email?: string;
    epikForeisFlag?: string;
    epidForeisFlag?: string;
    message?: string;
};
export declare type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | ErrorRecord
 */
export declare const getIdentity: (afm: string, user: string, pass: string, overrides?: Overrides | undefined) => Promise<any>;
export default getIdentity;
