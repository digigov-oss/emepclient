import soapClient from "./soapClient.js";
import {
  generateAuditRecord,
  AuditRecord,
  FileEngine,
  AuditEngine,
} from "@digigov-oss/gsis-audit-record-db";
import config from "./config.json";

export declare type AuditInit = AuditRecord;
export declare type GetNncIdentityOutputRecord = {
  countryCode?: string;
  countryDescr?: string;
  addressStreet?: string;
  addressNumber?: string;
  addressCity?: string;
  addressZipCode?: string;
  telephone?: string;
  countryCode2?: string;
  countryDescr2?: string;
  addressStreet2?: string;
  addressNumber2?: string;
  addressCity2?: string;
  addressZipCode2?: string;
  telephone2?: string;
  mobile?: string;
  email?: string;
  epikForeisFlag?: string;
  epidForeisFlag?: string;
  message?: string;
};

export declare type ErrorRecord = {
  errorCode: string;
  errorDescr: string;
};

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type Overrides = {
  endpoint?: string;
  prod?: boolean;
  auditInit?: AuditRecord;
  auditStoragePath?: string;
  auditEngine?: AuditEngine;
};

/**
 *
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | ErrorRecord
 */
export const getIdentity = async (
  afm: string,
  user: string,
  pass: string,
  overrides?: Overrides | undefined
) => {
  const endpoint = overrides?.endpoint ?? "";
  const prod = overrides?.prod ?? false;
  const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
  const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
  const auditEngine =
    overrides?.auditEngine ?? new FileEngine(auditStoragePath);
  const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
  const auditRecord = await generateAuditRecord(auditInit, auditEngine);
  if (!auditRecord) throw new Error("Audit record is not initialized");

  try {
    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const Identity = await s.getIdentity(afm);
    return { ...Identity, ...auditRecord };
  } catch (error) {
    throw error;
  }
};

export default getIdentity;
